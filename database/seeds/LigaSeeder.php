<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LigaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ligas')->insert([
        	'nama' => 'Liga 1',
        	'negara' => 'Indonesia',
        	'gambar' => 'Liga 1.png',
        ]);

        DB::table('ligas')->insert([
        	'nama' => 'Liga 2',
        	'negara' => 'Indonesia',
        	'gambar' => 'Liga 2.png',
        ]);

        DB::table('ligas')->insert([
        	'nama' => 'Liga 3',
        	'negara' => 'Indonesia',
        	'gambar' => 'Liga 3.png',
        ]);

    }
}
